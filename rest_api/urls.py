from django.urls import path

from . import views

urlpatterns = [
    path('event/', views.EventView.as_view(),),
    path('event/<int:pk>', views.EventView.as_view(),),
]
