from rest_framework import serializers

from rest_api.models import User, Tariff, Event


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'current_balance',
            'date_added',
            'age',
            'city',
            'last_activity',
            'active_tariff',
        )


class TariffSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tariff
        fields = (
            'id',
            'name',
            'date_begin',
            'date_end',
            'included_minutes',
            'included_sms',
            'included_traffic',
        )


class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = (
            'id',
            'time_stamp',
            'user',
            'service_type',
            'units_spent',
        )
