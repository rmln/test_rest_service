from django.db import models


# В некоторых полях вместо DateField применено DateTimeField, по сути - это неправильно.

class User(models.Model):
    current_balance = models.FloatField(verbose_name='Текущий баланс', max_length=2000)
    date_added = models.DateTimeField(verbose_name='Дата добавления')
    age = models.IntegerField(verbose_name='Возраст')
    city = models.CharField(verbose_name='Город проживания', max_length=200)
    last_activity = models.DateTimeField(verbose_name='Временная метка последней активности')
    active_tariff = models.ForeignKey('rest_api.Tariff', verbose_name='Временная метка последней активности',
                                      on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Пользователи'


class Tariff(models.Model):
    name = models.CharField(verbose_name='Название', max_length=200)
    date_begin = models.DateTimeField(verbose_name='Дата начала действия')
    date_end = models.DateTimeField(verbose_name='Дата конца действия')
    included_minutes = models.IntegerField(verbose_name='Объем минут')
    included_sms = models.IntegerField(verbose_name='Объем смс')
    included_traffic = models.IntegerField(verbose_name='Объем трафика (мб)')

    class Meta:
        verbose_name = 'Тарифы'


class Event(models.Model):
    time_stamp = models.DateTimeField(verbose_name='Метка времени')
    user = models.ForeignKey('rest_api.User', verbose_name='Абонент', on_delete=models.PROTECT)
    service_type = models.CharField(verbose_name='Тип услуги (звонок, смс, трафик)', max_length=50)
    units_spent = models.IntegerField(verbose_name='Объем затраченных единиц')

    class Meta:
        verbose_name = 'События'
