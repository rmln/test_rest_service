from django.http import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Event
from .serializers import EventSerializer


# Тут один класс для работы с событиями. Очень базово. Копировать на работу
# с остальными моделями не стал.
class EventView(APIView):

    def get_object(self, pk):
        try:
            return Event.objects.get(pk=pk)
        except Event.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None):
        if not pk:
            serializer = EventSerializer(instance=Event.objects.all(), many=True)
        else:
            event = self.get_object(pk)
            serializer = EventSerializer(event)

        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = EventSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        event = self.get_object(pk)
        event.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
